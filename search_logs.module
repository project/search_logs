<?php

/**
 * @file
 */

use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Query\ResultSetInterface;
use Solarium\QueryType\Select\Result\Result;

/**
 * Lets modules alter the search results returned from a Solr search.
 *
 * @param \Drupal\search_api\Query\ResultSetInterface $result_set
 *   The results array that will be returned for the search.
 * @param \Drupal\search_api\Query\QueryInterface $query
 *   The SearchApiQueryInterface object representing the executed search query.
 * @param \Solarium\QueryType\Select\Result\Result $result
 *   The Solarium result object.
 *
 * @deprecated in search_api_solr:4.2.0 and is removed from
 *   search_api_solr:4.3.0. Handle the PostExtractResultsEvent instead.
 *
 * @see https://www.drupal.org/project/search_api_solr/issues/3203375
 * @see \Drupal\search_api_solr\Event\PostExtractResultsEvent
 */
function search_log_search_api_solr_search_results_alter(ResultSetInterface $result_set, QueryInterface $query, Result $result) {
  if ($result->count() === 0) {
    $search = $query->getOriginalKeys();
    if (empty($search)) {
      return;
    }
    \Drupal::service('search_log.logger')->log($search);
    // Deny page cache.
    // Drupal aggressively caches search api requests.
    \Drupal::service('page_cache_kill_switch')->trigger();
  }
}