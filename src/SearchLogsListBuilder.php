<?php

namespace Drupal\search_logs;

use Drupal\Core\Entity\EntityListBuilder;

/**
 * List builder for SearchLogItem entities.
 */
class SearchLogsListBuilder extends EntityListBuilder {

}
