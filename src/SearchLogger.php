<?php

namespace Drupal\search_logs;

use Drupal\search_logs\Entity\SearchLogs;

/**
 * Search logger service.
 */
class SearchLogger implements SearchLoggerInterface {

  /**
   * {@inheritDoc}
   */
  public function log($search_text) {
    // Check if the search text exists.
    $results = \Drupal::entityTypeManager()->getStorage('search_log')->loadByProperties(['search' => $search_text]);
    if (empty($results)) {
      // Create a new search log item.
      $search_logs = SearchLogs::create([
        'search' => $search_text,
        'count' => 1,
      ]);
      $search_logs->save();
    }
    else {
      // Increment the count.
      /** @var \Drupal\search_logs\Entity\SearchLogs $search_logs */
      $search_logs = reset($results);
      $search_logs->set('count', $search_logs->get('count')->value + 1);
      $search_logs->set('last_search', time());
      $search_logs->save();
    }

    return $search_logs;
  }

}
