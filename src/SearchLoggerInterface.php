<?php

namespace Drupal\search_logs;

/**
 * Search logger service interface.
 */
interface SearchLoggerInterface {

  /**
   * Logs a search.
   *
   * If the search text does not exist, it will be created. else the count will
   * be incremented.
   *
   * @param string $search_text
   *   The search text.
   *
   * @return \Drupal\search_logs\Entity\SearchLogs
   *   The search log.
   */
  public function log($search_text);

}
