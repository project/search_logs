<?php

namespace Drupal\search_logs\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the SearchLogItem entity.
 *
 * @ingroup search_logs
 *
 * @ContentEntityType(
 *   id = "search_logs",
 *   label = @Translation("SearchLogItem"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\search_logs\SearchLogItemListBuilder",
 *   },
 *   base_table = "search_logs",
 *   translatable = FALSE,
 *   admin_permission = "administer search log item entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "search",
 *   }
 * )
 */
class SearchLogs extends ContentEntityBase {

  /**
   * {@inheritDoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values['last_search'] = time();
  }

  /**
   * {@inheritDoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Search textfield.
    $fields['search'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Search'))
      ->setDescription(t('The search text.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 1024,
        'text_processing' => 0,
      ]);

    // Search count.
    $fields['count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Count'))
      ->setDescription(t('The number of times this search has been performed.'))
      ->setSettings([
        'default_value' => 0,
      ]);

    // Last search timestamp.
    $fields['last_search'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Last search'))
      ->setDescription(t('The timestamp of the last time this search was performed.'))
      ->setSettings([
        'default_value' => 0,
      ]);

    return $fields;
  }

}
